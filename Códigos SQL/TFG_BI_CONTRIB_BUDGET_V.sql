CREATE TABLE NGG.TFG_BI_CONTRIB_BUDGET_V(

NEW_REV 					DATE,

CONTROL_ID				FLOAT,

ORG_ID					FLOAT,

ORG_NAME					NVARCHAR (240 ),

DISTRIBUTION_TYPE		NVARCHAR (40 )	,

REV_TYPE					NVARCHAR (7 ),	

ACCOUNT_MANAGER		NVARCHAR (50 )	,

SHIP_TO_CUSTOMER		NVARCHAR (360 )	,

OAN						NVARCHAR (40 )	,

REGION					NVARCHAR (150 )	,

SALESGROUP_NAME		NVARCHAR (150 ),	

SALESGROUP_ID			FLOAT,	

BUSINESS_LINE			NVARCHAR (30 )	,

ACTIVITY_TYPE			NVARCHAR (150 ),	

TECHNOLOGY_CATEGORY	NVARCHAR (80 )	,

TECHNOLOGY_AREA		NVARCHAR (80 )	,

WIN_PROBABILITY		FLOAT	,

REV_PERIOD				NVARCHAR (5 )	,

REVENUE					FLOAT	,

REVQPPTO					FLOAT,	

REVYTDPPTO				FLOAT,	

REVYPPTO					FLOAT,	

COST						FLOAT,	

COSTQPPTO				FLOAT	,

COSTYTDPPTO				FLOAT	,

COSTYPPTO				FLOAT	,
		
MARGIN					FLOAT	,

MARGINQPPTO				FLOAT,	
	
MARGINYTDPPTO			FLOAT,	

MARGINYPPTO				FLOAT	

)